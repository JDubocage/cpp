#include "jeu.h"

// Constructeur
Jeu::Jeu(Grille* _grille): grille(_grille)
{
    this->initPartie();
}

// Destructeur : on lib�re la m�moire de l'objet grille
Jeu::~Jeu(){
    delete grille;
}

// Initialise une partie de Morpion ou Puissance4
// (Cr�ation des joueurs + lancement de la boucle de jeu)
void Jeu::initPartie() {

    // Cr�ation des deux joueurs
    std::string pseudoJ1, pseudoJ2;

    std::cout << "Veuillez entrer le pseudo du 1er joueur : " << std::endl;
    std::cin >> pseudoJ1;
    this->joueurUn = Joueur(Joueur::ID_JOUEUR_1, pseudoJ1);

    std::cout << "Veuillez entrer le pseudo du 2eme joueur : " << std::endl;
    std::cin >> pseudoJ2;
    this->joueurDeux = Joueur(Joueur::ID_JOUEUR_2, pseudoJ2);

    std::cout << std::endl;

    // C'est le joueur 1 qui commence
    this->joueurCourant = joueurUn;

    // Lancement de la boucle de jeu
    this->boucleDeJeu();
}

// Repr�sente la boucle de jeu principale de la partie
void Jeu::boucleDeJeu() {
    // Tant que la partie n'est pas termin�e
    while (!this->partieTerminee) {
        // Tour du joueur courant
        std::cout << "C'est au tour de " << this->joueurCourant.getPseudo() << " de jouer ! " << std::endl;
        // Le joueur courant fait son action
        this->joueTour();

        std::cout << std::endl;

        // Affichage de la grille
        std::cout << "Grille : " << std::endl;
        this->grille->Affichage();
        std::cout << std::endl;

        // On v�rifie si le joueur courant a gagn�
        if (this->grille->victoireJoueur(this->joueurCourant.getId())) {
            // On f�licite le gagnant
            this->feliciteGagnant(this->joueurCourant.getPseudo());
            // La partie est termin�e
            this->partieTerminee = true;
        }

        // On v�rifie si la grille est pleine
        if (this->grille->grillePleine()) {
            std::cout << "La grille est pleine : Match nul" << std::endl;
            // La partie est termin�e
            this->partieTerminee = true;
        }

        // Sinon on passe le tour � l'autre joueur
        this->finTour();
    }
}

// Repr�sente l'action effectu�e par le joueur courant lors de son tour
void Jeu::joueTour() {
    // Si c'est une partie de morpion
    if (dynamic_cast<GrilleMorpion*>(this->grille) != nullptr){
        GrilleMorpion* grilleMorpion = dynamic_cast<GrilleMorpion*>(this->grille);

        int ligne, colonne;
        bool saisieValidee = false;
        do {
            std::string choixLigne, choixColonne;
            std::cout << "Entrez les coordonnees ou vous voulez jouer (ligne-colonne)" << std::endl;
            std::cin >> choixLigne >> choixColonne;

            // V�rification de la saisie
            ligne = valideSaisie(choixLigne, grilleMorpion->NB_LIGNES) - 1;
            colonne = valideSaisie(choixColonne, grilleMorpion->NB_COLONNES) - 1;

            if (ligne != -1 && colonne != -1){
                saisieValidee = checkCaseVide(colonne, ligne);
            }

        }
        while (!saisieValidee);

        //On place le pion du joueur
        grilleMorpion->deposerJeton(this->joueurCourant.getId(), colonne, ligne);
    }

    // Si c'est une partie de puissance 4
    if (dynamic_cast<GrillePuissance4*>(this->grille) != nullptr){
        GrillePuissance4* grilleP4 = dynamic_cast<GrillePuissance4*>(this->grille);

        int ligne, colonne;
        bool saisieValidee = false;
        do {
            std::string choixColonne;
            std::cout << "Entrez la colonne ou vous voulez jouer" << std::endl;
            std::cin >> choixColonne;

            // V�rification de la saisie
            colonne = valideSaisie(choixColonne, grilleP4->NB_COLONNES) - 1;

            if (colonne != -1){
                ligne = grilleP4->premiereCaseVide(colonne);
                saisieValidee = ligne != -1 ? true : false;
            }
        }
        while (!saisieValidee);

        //On place le pion du joueur
        grilleP4->deposerJeton(this->joueurCourant.getId(), colonne, ligne);
    }

}

// Valide la saisie de l'utilisateur
int Jeu::valideSaisie(const std::string saisie, const int borne) const {
    // On v�rifie que la saisie est bien un nombre qu'on peut utiliser
    int nombre = valideNombre(saisie);
    if (nombre != -1){
        // On verifie que la valeur est bien comprise dans les bornes
        return valideBornes(nombre, borne);
    }
    return -1;
}

// V�rifie si une saisie peut �tre convertie en entier et renvoie sa valeur (ou -1)
int Jeu::valideNombre(const std::string choix) const {
    int valChoix;
    try {
        valChoix = std::stoi(choix);
    }
    catch (std::invalid_argument const& ex) {
        std::cout << "Saisie incorrecte, veuillez entrer un entier !" << std::endl;
        return -1;
    }
    return valChoix;
}

// V�rifie si un nombre donn� est compris entre 0 et une borne puis renvoie sa valeur (ou -1)
int Jeu::valideBornes(const int val, const int borne) const {
    if (val < 0 || val > borne) {
        std::cout << "Saisie incorrecte (d�passe les bornes) " << std::endl;
        return -1;
    }
    return val;
}

// V�rifie si une case donn�e de la grille est vide
bool Jeu::checkCaseVide(const int x, const int y) const {
    if (!this->grille->caseVide(x, y)) {
        std::cout << "La case n'est pas vide !" << std::endl;
        return false;
    }
    return true;
}

// Affiche un message de f�licitation sur la console
void Jeu::feliciteGagnant(const std::string pseudoGagnant) const {
    std::cout << "Felicitations " << pseudoGagnant << " !" << std::endl;
    std::cout << "Tu es le vainqueur de la partie" << std::endl;
}

// M�thode qui passe le tour au joueur suivant
void Jeu::finTour() {
    if (joueurCourant.getId() == joueurUn.getId()) {
        joueurCourant = joueurDeux;
    }
    else joueurCourant = joueurUn;
}
