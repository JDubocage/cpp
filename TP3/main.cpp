#include <QCoreApplication>
#include <iostream>
#include <stdbool.h>
#include "jeu.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    int choix;
    bool saisieCorrecte = false;

    // Affichage du menu
    std::cout << "********** Selection du Jeu **********" << std::endl;
    std::cout << "1. Morpion" << std::endl;
    std::cout << "2. Puissance 4" << std::endl;

    // Boucle de choix tant que la saisie n'est pas correcte
    do {
        std::cout << "Veuillez faire votre choix : ";
        std::cin >> choix;
        if (choix != 0 && choix != 1 && choix != 2){
            std::cout << "Saisie incorrecte" << std::endl;
        }
        else saisieCorrecte = true;
    }
    while (!saisieCorrecte);

    std::cout << std::endl;

    // Traitement du choix
    switch (choix) {
        case (1):
            Jeu(new GrilleMorpion);
            break;
        case (2):
            Jeu(new GrillePuissance4);
            break;
    }
}
