# Module : Développement C++
## TP 03 : Morpion et Puissance 4

## Principes SOLID
[Dépot Julien](https://gitlab.com/JDubocage/cpp)

[Dépot Benjamin](https://github.com/HAMMBE/A1_CPP/tree/main/TP3/TP3)

### **S - Single Responsibility**
Ce principe préconise l'unique responsabilité de chaque classe.
Au niveau de ce principe, le projet de Julien semble correct. 
Les responsabilités sont biens séparées dans différentes classes : 
- toutes les opérations liées aux opérations sur les grilles sont séparées dans leurs fichiers respectifs (ajout de jeton dans une position donnée par exemple)
- La classe jeu s'occupe de la gestion des tours et le joueurs sont des instances d'une classe.
Dans le projet de Benjamin, les classes Jeu et Joueur ne sont pas distinctes, la classe Jeu a donc deux responsabilité :

*Projet de Benjamin*
```cpp
public:
	Jeu();
	void jeuMorpion();
	void jeuP4();
private :
	bool fin;
	GrilleMorpion morpion;
	GrilleP4 P4;
	void felicitation(int idJoueur);
};
```
*Projet de Julien*
```cpp
class Joueur
{
public:
    Joueur();
    Joueur(int _id, std::string _pseudo);s
    inline int getId() const{ return id; }
    inline std::string getPseudo() const{ return pseudo; }
private:
    int id;
    std::string pseudo;
};
```


### **O - Open-Closed**
Ce principe stipule qu'on peut étendre (ajouter des fonctionalités) le comportement d'une classe donnée sans en changer le comportement initial.

Dans le cas du projet de Benjamin, la logique de jeu du puissance 4 ou du morpion est englobée dans des fonctions spécfiques (`JeuP4()` et `JeuMorpion()`) alors on pourrait techniquement rajouter de nouvelle méthodes sans modifier du code existant mais on se retrouve on se retrouve avec beacuoup de code dupliqué. On doit également modifier le fichier Main qui s'occuper de la sélection du jeu.
```cpp
class Jeu
{
public:
	Jeu();
	void jeuMorpion();
	void jeuP4();
private :
	bool fin;
	GrilleMorpion morpion;
	GrilleP4 P4;
}
```
*Fichier main*
```cpp
int main()
{
    if (jeulancer == 1) {
		jeu1.jeuMorpion();
	}
	else {
		jeu1.jeuP4();
	}
}
```

Dans le projet de Julien le principe O n'est pas bien appliqué non plus, la classe de Jeu vérifie le type de la grille lors du tours du joueur :
*Classe jeu*
```cpp
// Attributs
    Grille* grille;

void Jeu::joueTour() {
    // Si c'est une partie de morpion
    if (dynamic_cast<GrilleMorpion*>(this->grille) != nullptr){
        ...
    }

     // Si c'est une partie de puissance 4
    if (dynamic_cast<GrillePuissance4*>(this->grille) != nullptr){
        ...
    }
}
```
En effet, si nous devions créer une nouvelle classe étendant la classe Grille, il faudrait modifier la classe jeu et rajouter une conditionelle pour faire fonctionner le code

### **L -  Liskov Substitution**
Ce principe impose le fait que toute classe mère peut être substituée par une de ses classes filles sans que le comportement soit altéré.
Dans notre cas, puisque le TP de Benjamin n'utilise pas d'abstraction ni d'héritage ce principe n'est pas vérifiable.

Dans le projet de Julien, les classe `GrilleMorpion` et `GrillePuissance4` étendent la classe abstraite `Grille` qui réunit les comportements communs. La classe `Jeu` attends un type ``Grille` mère et que l'on passe un Puissance 4 ou un Morpion, cela va fonctionner. Le principe est donc respecté.

```cpp
class Jeu
{
public:
    Jeu(Grille* _grille);
    ~Jeu();
private:
    Grille* grille;
    void initPartie();
    void boucleDeJeu();
    void joueTour();
    ...
};
```

### **I — Interface Segregation**
Ce principe requiert que les classes ne doivent implémenter uniquement les méthodes (et donc les interfaces) qui leurs sont nécessaires. Pour ce faire les interfaces doivent êtres les plus concises et précises possible. 
Dans notre cas, puisque le TP de Benjamin n'utilise pas d'abstraction ni d'héritage ce principe n'est pas vérifiable.

Dans le projet de Julien, l'abstraction fournie dans la classe `Grille` impose aux enfants d'implémenter les méthodes `ligneComplete()`, `colonneComplete()`, et `diagonaleComplete()` pour pouvoir centraliser la vérification de victoire.
Cependant, imaginons que nous voulions implémenter un jeu basé sur où grille ou les diagonales ne sont pas une condition de victoire, nous devrions implémenter cette méthode alors qu'elle nous serait inutile.

```cpp
class Grille {
    bool victoireJoueur(const int idJoueur) const;
    virtual bool ligneComplete(const int idJoueur, const int x) const = 0;
    virtual bool colonneComplete(const int idJoueur, const int y) const = 0;
    virtual bool diagonaleComplete(const int idJoueur, const int diag) const = 0;
}
```

### **D - Dependency Inversion**
Pour ce principe, les classes de haut niveau ne doit pas dépendre de classes de bas niveau mais d'abstraction.
Pour le TP de Benjamin, il n'existe que des classes de bas niveaux car il n'utilise pas d'abstraction ni d'héritage : le principe n'est pas respécté.
Dans le TP de Julien on peut faire la distinction entre des classes de bas niveaux (`GrilleMorpion` / `GrillePuissance4`) et la classe de haut niveau : `Grille`.
Cependant les classes communiquent directement sans passer par des interfaces, ce principe n'est pas respecté.

### **Loi de Démeter**
Nos deux projets semblent respecter la loi de Démeter, nous ne passont pas par des ponts pour accéder à des objets (puis méthodes / attributs). On accède au maximum aux attributs ou au méthodes de la grille contenue dans le Jeu.

### **Tell don't ask**
L'objectif de ce principe est de ne pas interroger un autre objet pour avoir une réponse sur son propre état. 

Le projet de Benjamin ne le respecte pas :

```cpp
void Jeu::jeuP4(){
    if (P4.victoireJoueur(1)) {
		fin = true;
		felicitation(1);
    }
}
```
On fait une verification sur le P4 depuis la classe jeu pour savoir si notre partie est terminée.

Dans le projet de Julien on retrouve la même erreur :

```cpp
void Jeu::boucleDeJeu() {
        if (this->grille->victoireJoueur(this->joueurCourant.getId())) {
            // On félicite le gagnant
            this->feliciteGagnant(this->joueurCourant.getPseudo());
            // La partie est terminée
            this->partieTerminee = true;
        }
}
```
