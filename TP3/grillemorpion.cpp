#include "grillemorpion.h"

GrilleMorpion::GrilleMorpion(): Grille(NB_LIGNES_MORPION, NB_COLONNES_MORPION) {}

// V�rifie si une ligne donn�e de la grille est compl�te
bool GrilleMorpion::ligneComplete(const int idJoueur, const int x) const {
    for (int i = 0; i < NB_COLONNES; i++){
        if (this->grille[x][i] != idJoueur){
            return false;
        }
    }
    return true;
}

// V�rifie si une colonne donn�e de la grille est compl�te
bool GrilleMorpion::colonneComplete(const int idJoueur, const int y) const {
    for (int i = 0; i < NB_LIGNES; i++){
        if (this->grille[i][y] != idJoueur){
            return false;
        }
    }
    return true;
}

// V�rifie si une des deux diagonales de la grille est compl�te
bool GrilleMorpion::diagonaleComplete(const int idJoueur, const int diag) const {
    if (diag == DIAGONALE_DESCENDANTE) {
        for (int i = 0, j = 0; i < NB_COLONNES && j < NB_LIGNES; i++, j++) {
            if (this->grille[i][j] != idJoueur) {
                return false;
            }
        }
        return true;
    }
    else if (diag == DIAGONALE_ASCENDANTE) {
        for (int i = NB_COLONNES - 1, j = 0; i > 0 && j < NB_LIGNES; i--, j++) {
            if (this->grille[i][j] != idJoueur) {
                return false;
            }
        }
        return true;
    }
    return false;
}

