#include "grillepuissance4.h"

GrillePuissance4::GrillePuissance4(): Grille(NB_LIGNES_PUISSANCE4, NB_COLONNES_PUISSANCE4){}

// Simule un jeton qui tombe dans une colonne de la grille :
// Renvoie la première case vide en partant du bas de la colonne donnée (ou -1 si aucune n'est disponible)
int GrillePuissance4::premiereCaseVide(const int colonne){
    for (int i = NB_LIGNES - 1; i >= 0; i--){
        if (this->caseVide(colonne, i)){
            return i;
        }
    }
    return -1;
}

// Vérifie si une ligne donnée de la grille est complète
bool GrillePuissance4::ligneComplete(const int idJoueur, const int x) const {
    // On compte le nombre de jetons alinés sur la ligne
    int nbJetonsAlignes = 0;
    for (int i = 0; i < NB_COLONNES; i++){
        if (this->grille[x][i] == idJoueur){
            nbJetonsAlignes++;
        }
        else nbJetonsAlignes = 0;
    }

    // Si le nombre de jetons aligné est égal à 4 la ligne est complete
    if (nbJetonsAlignes == NB_JETONS_VICTOIRE){
        return true;
    }
    else return false;
}

// Vérifie si une colonne donnée de la grille est complète
bool GrillePuissance4::colonneComplete(const int idJoueur, const int y) const {
    // On compte le nombre de jetons alinés sur la colonne
    int nbJetonsAlignes = 0;
    for (int i = 0; i < NB_LIGNES; i++){
        if (this->grille[i][y] == idJoueur){
            nbJetonsAlignes++;
        }
        else nbJetonsAlignes = 0;
    }

    // Si le nombre de jetons aligné est égal à 4 la colonne est complete
    if (nbJetonsAlignes == NB_JETONS_VICTOIRE){
        return true;
    }
    else return false;
}

// Vérifie si une diagonale de la grille est complète
bool GrillePuissance4::diagonaleComplete(const int idJoueur, const int diag) const {
    if (diag == DIAGONALE_ASCENDANTE) {
        for (int i = 3; i < NB_LIGNES; i++){
            for (int j = 0; j < NB_COLONNES; j++){
                if (this->grille[i][j] == idJoueur && this->grille[i-1][j+1] == idJoueur && this->grille[i-2][j+2] == idJoueur && this->grille[i-3][j+3] == idJoueur){
                    return true;
                }
            }
        }
    }
    else if (diag == DIAGONALE_DESCENDANTE) {
        for (int i = 3; i < NB_LIGNES; i++){
            for (int j = 3; j < NB_COLONNES; j++){
                if (this->grille[i][j] == idJoueur && this->grille[i-1][j-1] == idJoueur && this->grille[i-2][j-2] == idJoueur && this->grille[i-3][j-3] == idJoueur){
                    return true;
                }
            }
        }
    }
    return false;
}


