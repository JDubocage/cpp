#include "grille.h"

Grille::Grille(const int nbLignes, const int nbColonnes): NB_LIGNES(nbLignes), NB_COLONNES(nbColonnes) {
    initGrille();
}

// Initialisation de toute les cases de la grille à la valeur de CASE_VIDE
void Grille::initGrille(){
    grille.assign(NB_LIGNES,std::vector<int>(NB_COLONNES, 0));
}

// Dépose un jeton dans la grille pour des coordonnées données
void Grille::deposerJeton(const int idJoueur, const int x, int y){
    this->grille[y][x] = idJoueur;
}

// Vérifie si une case à des coordonnées données est vide
bool Grille::caseVide(const int x, const int y) const{
    return this->grille[y][x] == CASE_VIDE;
}

// Vérifie si la grille est pleine
bool Grille::grillePleine() const {
    for (int i = 0; i < NB_COLONNES; i++) {
        for (int j = 0; j < NB_LIGNES; j++) {
            if (this->grille[i][j] == CASE_VIDE) {
                return false;
            }
        }
    }
    return true;
}

// Renvoie le charactère contenu dans une case (Joueur 1 = X, Joueur 2 = O, Vide = _)
char Grille::getCaseChar(const int x, const int y) const {
    int caseGrille = this->grille[y][x];
    switch (caseGrille)
    {
    case Joueur::ID_JOUEUR_1:
        return 'X';
    case Joueur::ID_JOUEUR_2:
        return 'O';
    case CASE_VIDE:
        return '_';
    }
}

// Affiche le contenu de chacune des cases de la grille sur la console
void Grille::Affichage() const {
    for (int j = 0; j < NB_LIGNES; j++) {
        for (int i = 0; i < NB_COLONNES; i++) {
            std::cout << this->getCaseChar(i, j) << ' ';
        }
        //On renviens à la ligne
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

// Vérifie si un joueur donnée a rempli une des conditions de victoire
bool Grille::victoireJoueur(const int idJoueur) const {
    // On vérifie si le joueur a une ligne complète
    for (int i = 0; i < NB_LIGNES; i++) {
        if (this->ligneComplete(idJoueur, i)) {
            return true;
        }
    }
    // On vérifie si le joueur à une colonne complète
    for (int j = 0; j < NB_COLONNES; j++) {
        if (this->colonneComplete(idJoueur, j)) {
            return true;
        }
    }
    // On vérifie si le joueur a une diagonale complete
    if (this->diagonaleComplete(idJoueur, DIAGONALE_ASCENDANTE) || this->diagonaleComplete(idJoueur, DIAGONALE_DESCENDANTE)) {
        return true;
    }
}




