#include "jeupuissance4.h"

JeuPuissance4::JeuPuissance4() : grille()
{
    this->grille.initGrille();
}

void JeuPuissance4::boucleDeJeu() {
    // Tant que la partie n'est pas terminée
    while (!this->partieTerminee) {
        // Tour du joueur courant
        std::cout << "C'est au tour de " << this->joueurCourant.getPseudo() << " de jouer ! " << std::endl;
        std::cout << std::endl;

        // Affichage de la grille
        this->grille.Affichage();

        // Le joueur courant fait son action
        this->joueTour();

        std::cout << std::endl;

        // On vérifie si le joueur courant a gagné
        if (this->grille.victoireJoueur(this->joueurCourant.getId())) {
            this->grille.Affichage();
            // On félicite le gagnant
            this->feliciteGagnant(this->joueurCourant.getPseudo());
            // La partie est terminée
            this->partieTerminee = true;
        }
        // On vérifie si on peut continuer à jouer sur la grille ou non
        else if (this->grille.grillePleine()) {
            this->grille.Affichage();
            std::cout << "La grille est pleine : Match nul" << std::endl;
            // La partie est terminée
            this->partieTerminee = true;
        }
        // Sinon on passe le tour à l'autre joueur
        else this->finTour();
    }
}

// Représente l'action effectuée par le joueur courant lors de son tour
void JeuPuissance4::joueTour() {
    bool coupValide = false;
    // Tant que le joueur n'a pas joué un coup valide
    do {
        std::string choixColonne;
        std::cout << "Entrez la colonne ou vous voulez jouer :" << std::endl;
        std::cin >> choixColonne;

        // On vérifie que la saisie peut bien être convertie en nombre
        if (verifieType(choixColonne)){
            int colonne = this->convertitChaine(choixColonne);

            // On vérifie que le coup est Valide puis on le joue
            coupValide = this->grille.jouerCoup(this->joueurCourant.getId(), colonne);
        }
    }
    while (!coupValide);
}


