#include "jeuothello.h"

JeuOthello::JeuOthello() : grille()
{
    this->grille.initGrille();
}

void JeuOthello::boucleDeJeu() {
    // Tant que la partie n'est pas terminée
    while (!this->partieTerminee) {
        // Tour du joueur courant

        // On vérifie que le joueur peut jouer
        if (!this->grille.peutJouer(this->joueurCourant.getId(), this->getIdAdversaire())) {
            // Si il ne peut pas, il passe son tour
            this->finTour();
        }

        std::cout << "C'est au tour de " << this->joueurCourant.getPseudo() << " de jouer ! " << std::endl;
        std::cout << std::endl;

        // Affichage de la grille
        this->grille.Affichage();

        // Le joueur courant fait son action
        this->joueTour();

        std::cout << std::endl;

        // On vérifie si un des joueurs a gagné
        int idGagnant = this->grille.joueurGagnant(this->joueurUn.getId(), this->joueurDeux.getId());
        if (idGagnant != -1) {
            this->grille.Affichage();
            // On affiche les scores des deux joueurs
            this->afficheScores();

            // On félicite le gagnant
            if (idGagnant == joueurUn.getId()) {
                this->feliciteGagnant(joueurUn.getPseudo());
            }
            else this->feliciteGagnant(joueurDeux.getPseudo());

            // La partie est terminée
            this->partieTerminee = true;
        }

        // Sinon on passe le tour à l'autre joueur
        else this->finTour();
    }
}

// Affiche le score des deux joueurs
void JeuOthello::afficheScores() const {
    std::cout << "Score de " << joueurUn.getPseudo() << " : ";
    std::cout << this->grille.getScore(joueurUn.getId()) << std::endl;

    std::cout << "Score de " << joueurDeux.getPseudo() << " : ";
    std::cout << this->grille.getScore(joueurDeux.getId()) << std::endl;
}

void JeuOthello::joueTour() {
    bool coupValide = false;
    // Tant que le joueur n'a pas joué un coup valide
    do {
        std::string choixLigne, choixColonne;
        std::cout << "Entrez les coordonnees ou vous voulez jouer (ligne-colonne) :" << std::endl;
        std::cin >> choixLigne >> choixColonne;

        // On vérifie que la saisie peut bien être convertie en nombre
        if (verifieType(choixLigne) && verifieType(choixColonne)){
            int ligne = this->convertitChaine(choixLigne);
            int colonne = this->convertitChaine(choixColonne);

            // On vérifie que le coup est Valide puis on le joue
            coupValide = this->grille.jouerCoup(this->joueurCourant.getId(),this->getIdAdversaire(), colonne, ligne);
        }
    }
    while (!coupValide);
}

int JeuOthello::getIdAdversaire() const {
    if (joueurCourant.getId() == joueurUn.getId()) {
        return joueurDeux.getId();
    }
    return joueurUn.getId();
}

