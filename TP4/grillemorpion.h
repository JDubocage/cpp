#ifndef GRILLEMORPION_H
#define GRILLEMORPION_H

#include <stdbool.h>
#include <array>
#include <iostream>
#include "grille.h"

class GrilleMorpion : public Grille
{
public:
    // Construteur
    GrilleMorpion();
    // Logique métier
    void initGrille();
    bool jouerCoup(const int idJoueur, const int x, const int y);
    bool victoireJoueur(const int idJoueur) const;

private:
    bool ligneComplete(const int idJoueur, const int x) const;
    bool colonneComplete(const int idJoueur, const int y) const;
    bool diagonaleComplete(const int idJoueur, const int diag) const;

    // Constantes de classe
    static const int NB_LIGNES_MORPION = 3;
    static const int NB_COLONNES_MORPION = 3;
};

#endif // GRILLEMORPION_H
