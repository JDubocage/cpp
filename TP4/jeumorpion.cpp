#include "jeumorpion.h"

JeuMorpion::JeuMorpion() : grille()
{
    this->grille.initGrille();
}

void JeuMorpion::boucleDeJeu() {
    // Tant que la partie n'est pas terminée
    while (!this->partieTerminee) {
        // Tour du joueur courant
        std::cout << "C'est au tour de " << this->joueurCourant.getPseudo() << " de jouer ! " << std::endl;
        std::cout << std::endl;

        // Affichage de la grille
        this->grille.Affichage();

        // Le joueur courant fait son action
        this->joueTour();

        std::cout << std::endl;

        // On vérifie si le joueur courant a gagné
        if (this->grille.victoireJoueur(this->joueurCourant.getId())) {
            this->grille.Affichage();
            // On félicite le gagnant
            this->feliciteGagnant(this->joueurCourant.getPseudo());
            // La partie est terminée
            this->partieTerminee = true;
        }

        // On vérifie si on peut continuer à jouer sur la grille ou non
        else if (this->grille.grillePleine()) {
            this->grille.Affichage();
            std::cout << "La grille est pleine : Match nul" << std::endl;
            // La partie est terminée
            this->partieTerminee = true;
        }

        // Sinon on passe le tour à l'autre joueur
        else this->finTour();
    }
}

// Représente l'action effectuée par le joueur courant lors de son tour
void JeuMorpion::joueTour() {
    bool coupValide = false;
    // Tant que le joueur n'a pas joué un coup valide
    do {
        std::string choixLigne, choixColonne;
        std::cout << "Entrez les coordonnees ou vous voulez jouer (ligne-colonne) :" << std::endl;
        std::cin >> choixLigne >> choixColonne;

        // On vérifie que la saisie peut bien être convertie en nombre
        if (verifieType(choixLigne) && verifieType(choixColonne)){
            int ligne = this->convertitChaine(choixLigne);
            int colonne = this->convertitChaine(choixColonne);

            // On vérifie que le coup est Valide puis on le joue
            coupValide = this->grille.jouerCoup(this->joueurCourant.getId(), colonne, ligne);
        }
    }
    while (!coupValide);
}



