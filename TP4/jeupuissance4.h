#ifndef JEUPUISSANCE4_H
#define JEUPUISSANCE4_H

#include "jeu.h"
#include "grillepuissance4.h"

class JeuPuissance4 : public Jeu
{
public:
    JeuPuissance4();
private:
    GrillePuissance4 grille;

    void boucleDeJeu();
    void joueTour();
};

#endif // JEUPUISSANCE4_H
