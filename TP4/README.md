# Module : Développement C++
## TP 04 : Morpion / Puissance 4 / Othello

Projet en binôme implementant plusieurs jeux à 2 joueurs se basant sur une grille (Morpion, Puissance 4 et Othello).

Extension du TP3 trouvable [ici](https://gitlab.com/JDubocage/cpp/-/tree/main/TP3) avec le rapport sur les principes SOLID.

### Membres 
- Julien DUBOCAGE
- Benjamin HAMM