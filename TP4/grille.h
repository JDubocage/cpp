#ifndef GRILLE_H
#define GRILLE_H

#include <iostream>
#include <array>
#include <vector>
#include <stdbool.h>
#include "joueur.h"

enum Diagonale { DIAGONALE_ASCENDANTE, DIAGONALE_DESCENDANTE };

class Grille {
public:
    // Constructeur
    Grille(const int _nbLignes, const int _nbColonnes);

    //Getters
    inline int getNbLignes() const { return NB_LIGNES; }
    inline int getNbColonnes() const { return NB_COLONNES; }

    // Logique métier
    bool grillePleine() const;
    void Affichage() const;

    // Constantes de classe
    static const int CASE_VIDE = 0;

protected:
    // Attributs
    const int NB_LIGNES;
    const int NB_COLONNES;
    std::vector<std::vector<int>> grille;

    // Logique métier
    virtual void initGrille() = 0;
    bool caseVide(const int x, const int y) const;
    void placerJeton(const int idJoueur, const int x, const int y);

private:
    char getCaseChar(const int x, const int y) const;
};

#endif // GRILLE_H
