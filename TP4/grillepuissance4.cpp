#include "grillepuissance4.h"

GrillePuissance4::GrillePuissance4(): Grille(NB_LIGNES_PUISSANCE4, NB_COLONNES_PUISSANCE4){}

// Initialisation de toute les cases de la grille à la valeur de CASE_VIDE
void GrillePuissance4::initGrille() {
    grille.assign(NB_LIGNES, std::vector<int>(NB_COLONNES, 0));
}

// Simule un jeton qui tombe dans une colonne de la grille :
// Renvoie la première case vide en partant du bas de la colonne donnée (ou -1 si aucune n'est disponible)
int GrillePuissance4::premiereCaseVide(const int colonne) const { 
    for (int i = NB_LIGNES - 1; i >= 0; i--){
        if (this->caseVide(colonne, i)){
            return i;
        }
    }
    return -1;
}

bool GrillePuissance4::jouerCoup(const int idJoueur, const int colonne) {
    //On vérifie que le coup est bien compris entre les bornes du jeu
    if (colonne < 0 || colonne > NB_COLONNES) {
        std::cout << "Saisie incorrecte (le choix de colonne doit etre compris entre 1 et " << NB_COLONNES << ")" << std::endl;
        return false;
    }

    // On récupère la premère case vide disponible
    int ligne = premiereCaseVide(colonne);

    // On vérifie que la colonne n'est pas pleine
    if (ligne == -1){
        std::cout << "La colonne choisie est pleine" << std::endl;
        return false;
    }

    // Le coup est valide, on peut placer le jeton
    this->placerJeton(idJoueur, colonne, ligne);
    return true;
}

// Vérifie si un joueur donnée a rempli une des conditions de victoire
bool GrillePuissance4::victoireJoueur(const int idJoueur) const {
    // On vérifie si le joueur a une ligne complète
    for (int i = 0; i < NB_LIGNES; i++) {
        if (this->ligneComplete(idJoueur, i)) {
            return true;
        }
    }
    // On vérifie si le joueur à une colonne complète
    for (int j = 0; j < NB_COLONNES; j++) {
        if (this->colonneComplete(idJoueur, j)) {
            return true;
        }
    }
    // On vérifie si le joueur a une diagonale complete
    if (this->diagonaleComplete(idJoueur, DIAGONALE_ASCENDANTE) || this->diagonaleComplete(idJoueur, DIAGONALE_DESCENDANTE)) {
        return true;
    }

    return false;
}

// Vérifie si une ligne donnée de la grille est complète
bool GrillePuissance4::ligneComplete(const int idJoueur, const int x) const {
    // On compte le nombre de jetons alinés sur la ligne
    int nbJetonsAlignes = 0;
    for (int i = 0; i < NB_COLONNES; i++){
        if (this->grille[x][i] == idJoueur){
            nbJetonsAlignes++;
        }
        else nbJetonsAlignes = 0;
    }

    // Si le nombre de jetons aligné est égal à 4 la ligne est complete
    if (nbJetonsAlignes == NB_JETONS_VICTOIRE){
        return true;
    }
    else return false;
}

// Vérifie si une colonne donnée de la grille est complète
bool GrillePuissance4::colonneComplete(const int idJoueur, const int y) const {
    // On compte le nombre de jetons alinés sur la colonne
    int nbJetonsAlignes = 0;
    for (int i = 0; i < NB_LIGNES; i++){
        if (this->grille[i][y] == idJoueur){
            nbJetonsAlignes++;
        }
        else nbJetonsAlignes = 0;
    }

    // Si le nombre de jetons aligné est égal à 4 la colonne est complete
    if (nbJetonsAlignes == NB_JETONS_VICTOIRE){
        return true;
    }
    else return false;
}

// Vérifie si une diagonale de la grille est complète
bool GrillePuissance4::diagonaleComplete(const int idJoueur, const int diag) const {
    if (diag == DIAGONALE_ASCENDANTE) {
        for (int i = 3; i < NB_LIGNES; i++){
            for (int j = 0; j < NB_COLONNES; j++){
                if (this->grille[i][j] == idJoueur && this->grille[i-1][j+1] == idJoueur && this->grille[i-2][j+2] == idJoueur && this->grille[i-3][j+3] == idJoueur){
                    return true;
                }
            }
        }
    }
    else if (diag == DIAGONALE_DESCENDANTE) {
        for (int i = 3; i < NB_LIGNES; i++){
            for (int j = 3; j < NB_COLONNES; j++){
                if (this->grille[i][j] == idJoueur && this->grille[i-1][j-1] == idJoueur && this->grille[i-2][j-2] == idJoueur && this->grille[i-3][j-3] == idJoueur){
                    return true;
                }
            }
        }
    }
    return false;
}


