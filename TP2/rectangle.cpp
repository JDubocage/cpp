#include <iostream>
#include "rectangle.h"

// Constructeur par défaut
Rectangle::Rectangle(): longeur(), largeur() {}
// Constructeur avec paramètres
Rectangle::Rectangle(int _longeur, int _largeur): longeur(_longeur), largeur(_largeur) {}

void Rectangle::Afficher() const {
    std::cout << "Rectangle : " << std::endl;
    std::cout << "Perimètre : " << getPerimetre() << std::endl;
    std::cout << "Aire : " << getSurface() << std::endl;
}