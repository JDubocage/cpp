#include <iostream>
#include "point.h"

// Constructeur par défaut
POINT::POINT() : x(),y() {}
// Constructeur avec paramètres
POINT::POINT(float _x, float _y) : x(_x), y(_y) {}
// Méthode calculant la distance entre deux points
float POINT::calculeDistance(const POINT autre) const{
    return sqrt( (pow((static_cast<double>(autre.x) - x), 2) + pow((static_cast<double>(autre.y) - y), 2)) );
}

void POINT::Afficher() const {
    std::cout << "POINT [" << x << "," << y << "]" << std::endl;
}