#ifndef POINT_H
#define POINT_H

#include <math.h>
struct POINT
{
    // Attributs
    float x;
    float y;
    // Constructeur par défaut
    POINT();
    // Constructeur avec paramètres
    POINT(float _x, float _y);
    // Méthodes utilitaires
    float calculeDistance(const POINT autre) const;
    void Afficher() const;
};

#endif // POINT_H