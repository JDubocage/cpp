#include <iostream>
#include "cercle.h"

// Constructeur par défaut
Cercle::Cercle(): centre(), diametre(){}
// Constructeur avec paramètres
Cercle::Cercle(POINT _centre, int _diametre): centre(_centre), diametre(_diametre){}

bool Cercle::estSurCercle(const POINT point) const {
    return centre.calculeDistance(point) == getRayon();
}

bool Cercle::estInterieurDuCercle(const POINT point) const {
    return centre.calculeDistance(point) < getRayon();
}

float Cercle::getPerimetre() const { 
    return M_PI * diametre; 
}

float Cercle::getSurface() const { 
    return M_PI * pow(getRayon(), 2); 
}

void Cercle::Afficher() const {
    std::cout << "Cercle : " << std::endl;
    std::cout << "Perimètre : " << getPerimetre() << std::endl;
    std::cout << "Aire : " << getSurface() << std::endl;
}