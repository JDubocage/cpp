# Module : Développement C++
## TP 01

- Partie 1 : Manipulation de nombres dans `"/Manipulation_nombres"`
- Partie 2 : Jeu de Tennis dans `"/Jeu_Tennis"`
- Partie 3 : Inscription dans la console et récupération des saisies dans `"/Console_Saisie"`

Julien DUBOCAGE - FIP 1A