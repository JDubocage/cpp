#include "q4.h"

array<int, 20> GenereTab(){
    array<int, 20> tab;
    for (int i = 0; i < tab.size(); i++){
        tab[i] = rand() % 251;
    }
    return tab;
}

void AfficheTab(array<int, 20> tab){
    string str = "[";
    for (int i = 0; i < tab.size(); i++){
        str += to_string(tab[i]);
        str += ",";
    }
    str.erase(str.end()-1);
    str += "]";
    cout << str << endl;
}

void InverseNombres(int &a, int &b){
    int temp = a;
    a = b;
    b = temp;
}

void TriTabCroissant(array<int, 20> &tab, int const size){
    bool en_desordre = true;

    for (int i = 0; i < size && en_desordre; ++i){
        en_desordre = false;
        for (int j = 1; j < size; ++j){
            if (tab[j-1] > tab[j]){
                InverseNombres(tab[j-1], tab[j]);
                en_desordre = true;
            }
        }
    }
}

int main(){
    srand(time(0));
    array<int, 20> tab = GenereTab();
    AfficheTab(tab);
    TriTabCroissant(tab, tab.size());
    AfficheTab(tab);
}