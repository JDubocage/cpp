#include <array>
#include <string>
#include <iostream>
#include <stdlib.h>
using namespace std;

array<int, 20> GenereTab();
void AfficheTab(array<int, 20> tab);
void InverseNombres(int &a, int &b);
void TriTabCroissant(array<int, 20> &tab, int const size);
