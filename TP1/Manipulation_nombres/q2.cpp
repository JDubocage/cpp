#include <iostream>
void InverseNombres(int &a, int &b){
    int temp = a;
    a = b;
    b = temp;
}

int main(){
    int a = 5;
    int b = 8;
    InverseNombres(a,b);
    std::cout << "5,8 => " << a << "," << b << std::endl; 
}

