#include <iostream>
void RemplaceParSommeP(int a, int b, int *res){
    *res = a + b;
}

void RemplaceParSommeRef(int a, int b, int &res){
    res = a + b;
}

int main(){
    int resP;
    int resRef;
    RemplaceParSommeP(2,3,&resP);
    RemplaceParSommeRef(2,3,resRef);
    std::cout << "Pointeurs : 2,3 => " << resP << std::endl; 
    std::cout << "Reference : 2,3 => " << resRef << std::endl; 
}

